# Guia Nº7 - Alg

#Algoritmo de Prim para elementos de una matriz

El proyecto trata de buscar e imprimir el camino más corto entre los elementos de una matriz

#Pre-requisitos

Para poder instalar el sofware se necesita de un sistema operativo Linux como lo es Ubuntu (link de la instalacion https://ubuntu.com/download)

Es fundamental la instalacion del lenguaje C++ y sus componentes de compilacion en su version g++ 9.3.0 (vease el siguiente link https://hetpro-store.com/TUTORIALES/compilar-cpp-g-linux-en-terminal-leccion-1/)

Se necesitara la instalacion de un editor de texto/IDE como lo puede ser "Visual Studio Code". (vease el link para la instalacion https://ubunlog.com/visual-studio-code-editor-codigo-abierto-ubuntu-20-04/?utm_source=feedburner&utm_medium=%24%7Bfeed%2C+email%7D&utm_campaign=Feed%3A+%24%7BUbunlog%7D+%28%24%7BUbunlog%7D%29)

Por ultimo, para poder clonar el repositorio del sofware, es necesario tener instalado "Gitlab" correctamente.


#Comenzando  

Para poder acceder a este sofware se requerira de clonar el espacio de trabajo desde https://gitlab.com/B3nj4/guia-n-7-alg.git la siguiente combinacion en su terminal Linux

    git clone https://gitlab.com/B3nj4/guia-n-7-alg.git

Luego, se debera ejecutar el archivo "Makefile" con la finalidad de poder compilar nuestro programa. Para ello debera ingresar en su terminal Linux el siguiente comando

    make

Ya ejecutado el "Makefile" debera ejecutar el programa "main.cpp" para poder interactuar con el sofware, para ello es fundamental ingresar lo siguiente en su terminal Linux

    ./Programa (Ingrese tamaño de la matriz entera(int))

Por ejemplo, para obtener una matriz 5x5 debemos hacer lo siguiente

    ./Programa 5


De esta forma el usuario podra interactuar con el sofware

    Ingrese el nombre del nodo 1:  


#Construido con

Para la creacion de este proyecto se necesito del editor de texto Visual Studio Code y el lenguaje de programacion "C++" 


#Autor
    
    Benjamin Vera Garrido - Ingenieria Civil en Bioinformatica

