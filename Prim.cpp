#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstring>
using namespace std;

//Librerias
#include "Prim.h"
#define max 99999


// Constructor
Prim::Prim() {}

// Inicializaciones
// inicializa un vector. recibe el vector como un puntero.
void Prim::inicializar_vector_caracter(char *vector, int n) {
    int col;
    // Crea el espacio de cada elemento por elemento filasxcolumnas
    // recorre el vector.
    for (col=0; col<n; col++) {
        vector[col] = ' ';
    }
}

// Inicializa matriz nxn. recibe puntero a la matriz.
void Prim::inicializar_matriz_enteros(int **matriz, int n) {
    // Crea el espacio de cada elemento por elemento filasxcolumnas
    for (int fila=0; fila<n; fila++) {
        for (int col=0; col<n; col++) {
            matriz[fila][col] = -1;
        }
    }
}

// inicializa vectores de naturaleza int (en este caso el vector D seria el unico int)
void Prim::inicializar_vector_entero(int *D, int **matriz, int n) {
    int col;
    // Crea elemento por elemento por las columnas
    for (col=0; col<n; col++) {
        D[col] = matriz[0][col];
    }
} 


// Impresiones
// Imprime un vector el cual recibira como puntero (de naturaleza string/char)
void Prim::imprimir_vector_caracter(char *vector, int n) {
    cout << endl;
    //REcorre e imprime elemento por elemento
    for (int i=0; i<n; i++) {
        cout << "vector[" << i << "]: " << vector[i] << endl;
    }
    
}

// imprime matriz.
void Prim::imprimir_matriz(int **matriz, int n) {
    cout << endl;
    //REcorre e imprime elemento por elemento de la forma nxn
    for (int fila=0; fila<n; fila++) {
        for (int col=0; col<n; col++) {
            cout << matriz[fila][col] << " "; 
        }
        cout << endl;
    }
    cout << endl;
}

// Imprime un vector el cual recibira como puntero (de naturaleza int, en este caso es exclusivo para el D)
void Prim::imprimir_vector_entero(int *vector, int n){
    int i;
    cout << endl;
    //REcorre e imprime elemento por elemento
    for (i=0; i<n; i++) {
        cout << "D["<< i <<"]: " << vector[i] << endl;
    }
    
} 



// Llenado/ingreso de los datos a la matriz
// Se agregan los nombres de los nodos a la matriz en un vector nombre[]
void Prim::agregar_nombres(int n, char nombres[], int **matriz) {
    char nombre;
    int i = 0;

    cout << endl;
    while(i < n){
        cout << "Ingrese el nombre del nodo " << i+1 << ": ";
        cin >> nombre;
        nombres[i] = nombre;
        i++;
    }
}

// Se ingresan las distancias de cada nodo (y como interactuan entre ellas)
void Prim::llenar_matriz(int n, char nombres[], int **matriz){ 
    int distancia;
    
    
    /* Llenar la matriz */
    for (int i=0; i<n; i++){
        cout << endl;
        cout << "Trabajando con el nodo: " << nombres[i];
        cout << endl;
        
        for (int j=0; j<n; j++){
            // Se debe obtener matriz simetrica (no dirigido)
            if(i != j && matriz[i][j] != 0 /* i > j*/){  // NO funciona condicion i > j para no ingresar los valores dos veces( grafo dirigido)
                cout << "Ingrese la distancia al nodo " << nombres[j] << ": ";
                cin >> distancia;
                
                matriz[i][j] = distancia;
                //matriz[j][i] = distancia; // NO funciona igualacion de [j][i] para no ingresar los valores dos veces( grafo dirigido)
                
            }
            else{
                /* Diagonal de la matriz*/
                matriz[i][j] = 0;
            }
            
        }
    }

}


// Algoritmo Prim
// Evaluar la menor distancia entre nodos
int Prim::menor_distancia(int *valor_peso, bool *seleccion, int n){
    int min = max,min_aux;
    //int max = min;
    //int min_aux = min;

    // Evaluacion elemento por elemento de la distancia mas corta entre los nodos adyacentes
    for(int i=0; i<n; i++){
        if(seleccion[i] == false && valor_peso[i] < min){
            min = valor_peso[i], min_aux = i;
        }
    }
    return min_aux;
}

// Busqueda del camino mas corto
void Prim::aplicar_prim(int **matriz, int n, char *vector){
    int padre[n]; // SImil al V, es el conjunto de v´ertices de G
    int valor_peso_prim[n]; //Simil al U, es un subconjunto propio del conjunto V, siendo su valor inicial el del primer v´ertice.
    bool seleccion_prim[n];  // Saber si se ha seleccionado o no

    /*
    Mientras (V <> U) Repetir
    Elegir una arista (u,v) ∈ A(G) tal que su costo
    sea minimo, siendo u∈U y v∈(V = U)
    Agregar la arista (u, v) a L
    Agregar el nodo v a U  */

    for(int i=0; i<n; i++){
        valor_peso_prim[i] = max, seleccion_prim[i] = false;
    }

    valor_peso_prim[0] = 0; //U es un subconjunto propio del conjunto V, siendo su valor inicial el del primer v´ertice.
    padre[0] = -1;

    for(int j=0; j<n-1; j++){
        int aux = menor_distancia(valor_peso_prim, seleccion_prim, n);
        seleccion_prim[aux] = true;
        for(int z=0; z < n ; z++){
            if(matriz[aux][z] && seleccion_prim[z] == false && matriz[aux][z] < valor_peso_prim[z]){
                padre[z] = aux,valor_peso_prim[z] = matriz[aux][z];
            }
        }
    }
    
    // IMprime el camino mas corto entre nodos determinados
    imprimir_prim(padre, matriz, n, vector); 

    // Muestra y crea el grafo del camino mas corto
    imprimir_grafo2(matriz, padre, valor_peso_prim, vector, n);

}

// IMpresion de los datos del camino mas corto
void Prim::imprimir_prim(int *padre, int **matriz, int n, char *vector){
    int distancia_total = 0;

    cout << "Borde - Distancia" << endl;
    
    // Simil al L es la lista de aristas que se va formando con las arista de menor costo que se van seleccionando.Valores de los caminos más cortos
    for(int i=1;i<n;i++){
        cout << padre[i] << "-" << i << "\t  " << matriz[i][padre[i]] << endl;  // ixpadre[i]
        distancia_total = distancia_total + matriz[i][padre[i]];
    }
    
    // Distancia total del camino mas corto
    cout << "Distancia total: " << distancia_total << endl;

}


// Impresion y creaciond el grafo
void Prim::imprimir_grafo(int **matriz, char *vector, int n) {
    ofstream fp;

    fp.open("grafo.txt", ios::out);
    fp << "graph G {" << endl;
    fp << "graph [rankdir = LR]" << endl;
    fp << "node [style=filled fillcolor=yellow]" << endl << endl;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++){
            if (i != j && i < j && matriz[i][j]) {
                fp << vector[i] << " -- " << vector[j] << " ";
                fp << "[label=" + to_string(matriz[i][j]) << "];" << endl;
            }
        }
    }

    fp << "}" << endl;
    fp.close();

    system("dot -Tpng -ografo.png grafo.txt &");
    system("eog grafo.png &");
}

// Impresion del grafo del camino mas corto

void Prim::imprimir_grafo2(int** matriz, int* padre, int *valor_peso, char *vector, int n) {

	int i;

	ofstream fp;

	fp.open("grafo2.txt", ios::out);
	fp << "graph G {" << endl;
	fp << "graph [rankdir = LR]" << endl;
	fp << "node [style=filled fillcolor=yellow]" << endl << endl;

	for(i = 1; i < n; i++) {
		fp << vector[padre[i]] << " -- " << vector[i] << " ";
		fp << "[label=" + to_string(valor_peso[i]) << "];" << endl;
	}

	fp << "}" << endl;
	fp.close();

	system("dot -Tpng -ografo2.png grafo2.txt &");
	system("eog grafo2.png &");
};

