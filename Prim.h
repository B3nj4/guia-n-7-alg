#ifndef Prim_H
#define Prim_H

//Librerias
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

// Clase Prim para las funciones
class Prim {
    private:
        
    public:
        //Constructor
        Prim();
        
        // Inicializadores
        void inicializar_vector_caracter (char *vector, int n);
        void inicializar_matriz_enteros(int **matriz, int n);
        void inicializar_vector_entero(int *D, int **matriz, int n);

        // Impresiones
        void imprimir_vector_caracter(char *vector, int n);  
        void imprimir_matriz(int **matriz, int n);
        void imprimir_vector_entero(int *vector, int n);
        
        // Ingreso de datos a al matriz
        void agregar_nombres(int n, char nombres[], int **matriz);
        void llenar_matriz(int n, char nombres[], int **matriz);

        // Algoritmo
        int menor_distancia(int *valor_peso, bool *seleccion, int n);
        void aplicar_prim(int **matriz, int n, char *vector);
        void imprimir_prim(int *padre, int **matriz, int n, char *vector);
        
        // Creacion e impresion del grafo (.png)
        void imprimir_grafo(int **matriz, char *vector, int n);
        void imprimir_grafo2(int **matriz, int *padre, int *valor_peso, char *vector, int n);

};
#endif