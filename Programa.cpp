//Librerias
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

#include "Prim.h"

// Funcion principal del programa
int main(int argc, char **argv) {

    // Variables y objetos
    int n;
    Prim prim;

    // Convierte string a entero.
    n = atoi(argv[1]);
    
    // Valida cantidad de parámetros mínimos.
    if (n <= 2) {
        cout << "Uso: \n./matriz n" << endl;
        return -1;
    }

    // Instancian vectores (enteros(D) y string(V, S, VS))
    char V[n]; 

    // Creacion de la matriz y el llenado de sus elementos (nxn de enteros).
    int **matriz;
    matriz = new int*[n];
    
    for(int i=0; i<n; i++){
        matriz[i] = new int[n];
    }

    // Inicializa vectores.
    prim.inicializar_vector_caracter(V, n);
    
    // INiciaizar matriz y su llenado
    prim.inicializar_matriz_enteros(matriz, n);
    prim.agregar_nombres(n, V, matriz);
    prim.llenar_matriz(n, V, matriz);
    prim.imprimir_matriz(matriz, n);

    // Se crea e imprime el grafo (.png)
    prim.imprimir_grafo(matriz, V, n);
    
    // Ya con la matriz impresa, se aplica el algoritmo de prim e imprime grafo
    prim.aplicar_prim(matriz, n, V);
    
    return 0;
}